class User < ApplicationRecord
    has_many :tickets, dependent: :destroy
    has_secure_password
    validates_presence_of :username, :password_digest
end