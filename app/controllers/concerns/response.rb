module Response
    def json_response(options ={})
      render json: {status:options[:status]||200, data:options[:data], message:options[:message]||Message.success, error:0}
    end
end