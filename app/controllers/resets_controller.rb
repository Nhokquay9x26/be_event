class ResetsController < ApplicationController
    skip_before_action :authorize_request
    #Reset DB 
    def index 
        User.destroy_all
        Event.destroy_all
        json_response(:message => "Clear DB Successfuly")
    end
end
