# Event controller
class EventsController < ApplicationController
    before_action :set_event,  only: [:show, :update, :destroy]
    
    def index
        events = Event.all.page(params[:page] || 1).per_page(params[:limit] || 20)
        total_page = Event.all.page(params[:page]).total_pages()
        json_response(:data=>{events:events, total_page:total_page}, :message => Message.get_events_success)
    end

    def create
        if current_user.username == "admin"
            event = Event.create!(event_params)
            json_response(:data => event, :message => Message.created_event)
        else
            json_response(:message => Message.account_not_created_event)
        end
    end

    def show
        if @event
            json_response(:data => @event)
        else
            json_response(:message => Message.event_not_exits)
        end
    end

    def update
        if @event
            @event.update(event_params) 
            json_response(:data => @event, :message => Message.updated_event)
        else
            json_response(:message=> Message.event_not_exits)
        end
    end

    def destroy
        if @event
            @event.destroy if @event
            json_response(:message=> Message.deleted_event)
        else
            json_response(:message=> Message.event_not_exits)
        end
    end

    private

    def event_params
        params.permit(:name, :description, :ticket_total, :ticket_price, :start_date, :end_date, :ticket_start_date, :ticket_end_date)
    end

    def set_event
        @event = Event.find_by(id: params[:id])
    end
end
