class TicketsController < ApplicationController

    before_action :set_event, :check_ticket,  only: [:create]
    
    def index 
        ticket = Ticket.where(user_id: current_user.id).page(params[:page] || 1)
        data = ticket.per_page(params[:limit] || 20)
        total_page = ticket.total_pages()
        json_response(:data=>{tickets:data, total_page:total_page}, :message => Message.get_tickets_success)
    end

    def create
        @ticket = Ticket.create!(ticket_params) if @event
            if @ticket 
                @event.update_attributes ticket_buy: (@event.ticket_buy + params[:quantity])
            json_response(:data => @ticket, :message => Message.buy_ticketed_success)
        end
    end

    private

    def ticket_params
        params.permit(:event_id, :quantity, :card_type, :card_number, :card_expiration, :cvc_code).merge(user_id: current_user.id)
    end

    def check_ticket
        if @event
            quantity = params[:quantity]
            if quantity > (@event.ticket_total - @event.ticket_buy)
                json_response(:message => Message.buy_ticketed_error_quantity)
            end
        else 
            json_response(:message => Message.event_not_exits)
        end
    end

    def set_event
        @event = Event.find_by(id:params[:event_id])
    end
end