# Auth controller
class AuthsController < ApplicationController
  skip_before_action :authorize_request
  before_action :check_user, only: [:create]
  def login
    auth_token = AuthenticateUser.new(auth_params[:username], auth_params[:password]).call
    json_response(data: auth_token, message: Message.login_success)
  end

  def create
    if @user
      json_response(message:Message.account_exits)
    else
      user = User.create!(user_params)
      auth_token = AuthenticateUser.new(user.username, user.password).call
      json_response(data:auth_token, message:Message.account_created)
    end
  end

  private

  def auth_params
    params.permit(:username, :password)
  end

  def user_params
    params.permit(
      :username,
      :password,
      :password_confirmation
    )
  end

  def check_user
    @user = User.find_by(:username => params[:username])
  end
end
