class Message
  def self.not_found(record = 'record')
    "Sorry, #{record} not found."
  end

  def self.success
    'Success'
  end

  def self.invalid_credentials
    'Invalid credentials'
  end

  def self.invalid_token
    'Invalid token'
  end

  def self.missing_token
    'Missing token'
  end

  def self.unauthorized
    'Unauthorized request'
  end

  def self.account_created
    'Account created successfully'
  end

  def self.account_exits
    'Account exits'  
  end

  def self.login_success
    'Login successfully'
  end

  def self.account_not_created_event
    'Account not create Event'
  end

  def self.account_not_created
    'Account could not be created'
  end

  def self.expired_token
    'Sorry, your token has expired. Please login to continue.'
  end

  def self.created_event
    'Created Event Successfuly.'
  end

  def self.updated_event
    'Updated Event Successfuly.'
  end

  def self.deleted_event
    'Deleted Event Successfuly.'
  end

  def self.event_not_exits
    'Event not exits.'
  end

  def self.get_events_success
    'Get list events success'  
  end

  def self.get_tickets_success
    'Get list tickets success'  
  end

  def self.buy_ticketed_success
    'Buy ticket successfuly'  
  end

  def self.buy_ticketed_error_quantity
    'Buy ticket error. Ticket fully'  
  end
end
