Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :events , except: [:edit] do
    resources :tickets , only:[:index, :create]
  end
  get 'resets', to: 'resets#index'
  post 'auth/login', to: 'auths#login'
  post 'auth/register', to: 'auths#create'
end