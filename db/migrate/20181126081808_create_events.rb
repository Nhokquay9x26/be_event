class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :name
      t.string :description
      t.string :banner
      t.integer :ticket_price
      t.integer :ticket_total
      t.integer :ticket_buy, default: 0
      t.date :start_date
      t.date :end_date
      t.date :ticket_start_date
      t.date :ticket_end_date

      t.timestamps
    end
  end
end
