class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.integer :user_id
      t.integer :event_id
      t.integer :quantity
      t.string :card_type
      t.string :card_number
      t.string :card_expiration
      t.string :cvc_code

      t.timestamps
    end
  end
end
